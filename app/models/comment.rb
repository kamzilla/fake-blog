class Comment < ActiveRecord::Base
  belongs_to :article

  def self.seed_fake_comments
    0.upto(5) do 
      c = Comment.new
      c.name = Faker::Name.name
      c.body = Faker::Hacker.say_something_smart
      c.save
    end    
  end
end
