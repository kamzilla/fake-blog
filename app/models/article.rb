# Public: Methods for generating content within the app.
class Article < ActiveRecord::Base
  has_many :comments, dependent: :destroy

  # Public: Creates a new Comment, associated with an article
  # 
  # Example: 
  # 
  #   a = Article.first
  #   a.create_random_comment
  #   # => <Comment id: 111, name: "Tre Hahn", body: "The SQL system is down, 
  #         copy the multi-byte bus so...", article_id: 4, created_at: 
  #         "2015-03-02 17:07:54", updated_at: "2015-03-02 17:07:54">
  # 
  # Returns a Comment object, with id, name, body, article_id, and timestamps
  def create_random_comment
    self.comments.create(name: Faker::Name.name, body: Faker::Hacker.say_something_smart)
  end

  # Public: Creates a new Article
  # 
  # Example: 
  # 
  #   b = Article.create_random_article
  #   # => [["title", "alias ut"], ["body", "Iure reiciendis libero perferendis
  #         repudiandae. Est vitae soluta dolores iusto quia. Voluptatem rerum
  #         cupidate est. ..."],  ["created_at", "2015-03-02 17:11:39.833066"], 
  #         ["updated_at", "2015-03-02 17:11:39.833066"]]
  # 
  # Returns a Nested array with the title, body, and times for the new article.
  def self.create_random_article
    a = Article.new
    a.title = Faker::Lorem.words(Random.rand(2..8)).join(" ")
    a.body = Faker::Lorem.paragraph(Random.rand(65..85))
    a.save
    a     
  end
end
