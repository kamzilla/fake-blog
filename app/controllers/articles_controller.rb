class ArticlesController < ApplicationController

  before_action :find_article, only: [:show, :create_comment, :destroy]
  before_action :set_cache_control_headers, only: [:index, :show]
  before_action :set_private, only: [:create_comment, :create_article]

  # All env, returns all Article objects.
  # In production, also sets surrogate key headers for the table_key and
  # record_key. "#{table_key}/#{article_id}"
  # 
  # Sets a table_key of 'articles', and a record_key for each article object.
  def index
    @articles = Article.all
    if Rails.env == "production"
      set_surrogate_key_header 'articles', @articles.map(&:record_key)
    end
  end

  # In production, sets a surrogate key for the current article.
  # 
  # 
  # Example: 
  # 
  #   Article[75]
  #   Response headers:
  #   Surrogate-Key:articles/75
  def show
    if Rails.env == "production"
      set_surrogate_key_header @article.record_key
    end
  end

  # Creates a new comment for current article
  # If in production, all cached versions of current article are purged,
  # using the record_key, "articles/#{article_id}"
  def create_comment
    @article.create_random_comment
    if Rails.env == 'production'
      @article.purge
    end
    redirect_to article_path(@article)
  end

  # Creates a new article, and if in production purges the cache. 
  def create_article
    new_article = Article.create_random_article
      if Rails.env == "production"
        new_article.purge_all
      end
    redirect_to articles_url
  end

  # Deletes the current article.
  # If in production, purges cache with matching record_key, and then purges
  # the cache with matching table_key.
  def destroy
    @article.destroy
    if Rails.env == "production"
      @article.purge
      @article.purge_all
    end
    redirect_to articles_path    
  end

  private

  def find_article
    @article = Article.find(params[:id])
    redirect_to articles_path unless @article
  end

  # Private: Sets Cache-Control headers to not store content
  # Used for create methods.
  def set_private
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = '-1'
  end
end
