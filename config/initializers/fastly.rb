FastlyRails.configure do |c|
  c.api_key = ENV['FASTLY_API_KEY']
  c.max_age = 2592000 # 30 days
  c.service_id = ENV['SERVICE_ID']  
end