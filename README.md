# Fake Blog

Fake Blog is an application set up to test caching with [Fastly](http://www.fastly.com/), using the [Fastly-Rails gem](https://github.com/fastly/fastly-rails). 

It uses the [Faker](https://github.com/stympy/faker) to generate content for both new articles and comments. 
